# TownSquare

## Description
This is the TownSquare for the Greentech.Berlin group in GitLab.

The project TownSquare can
also be seen as a meeting place, where people (members) gather
to explain and coordinate ideas and tasks in public.

Information about the relation of other projects in the Greentech.Berlin
context are preferably also explained here.


## Support
Any support is welcome, as the effort to expand this collection of
projects into time/space and beyond and to make it useful for
everyone will require quite an effort.


## Roadmap


## Contributing
Currently only me..


## License
The projects here shall be made open to the public.
If not specified otherwise, the Information in TownSquare are provided under the conditions described by [CC BY-NC-SA](https://creativecommons.org/licenses/by-sa/4.0/legalcode).
For other projects see their respective license. (For example the text used in the Wiki page "Satzung" is based on a document released by a third party under the [CC BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/).)


## Project status
